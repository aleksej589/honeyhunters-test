<?php

namespace app\controllers;

use app\models\Comment;
use Yii;
use yii\web\Controller;

class SiteController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSave()
    {
        $data = Yii::$app->request->post();
        $model = new Comment();
        $model->email = $data['email'];
        $model->name = $data['name'];
        $model->text = $data['text'];
        if ($model->save()) {
            return json_encode(['success' => true, 'comments' => Comment::find()->asArray()->all()]);
        } else {
            return json_encode(['success' => false, 'errors' => $model->getErrors(), 'model' => $model]);
        }

    }
    public function actionList()
    {
            return json_encode(['success' => true, 'comments' => Comment::find()->asArray()->all()]);

    }
}
