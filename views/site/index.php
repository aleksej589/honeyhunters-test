<?php

/* @var $this yii\web\View */

use yii\web\View;

$this->title = 'Тестовое задание';
?>
<link href="https://fonts.googleapis.com/css?family=Raleway:400,500" rel="stylesheet">

<div class="comments-container">
    <div class="comments-form-container">
        <div class="header">
            <div class="logo">
                <a href="//honey-hunters.ru"><img
                            src="data:image/webp;base64,UklGRioEAABXRUJQVlA4TB4EAAAvMUAMEO/GoG0kR+FP+tqUu38C8z//BJK2eNuvtm0bBl5T6DeAsaAHEkFidERx6BxcL8EMAxOPMAwEi+De40EEX3NbdgzHp01+CgiKtm2btqMR27Zt27Zt27ZZjm1WbNvmYzK+5txz731/ENH/CSDWCv3mLI9+xbyB1UnTapuf/TL2V/81ToMRP4xzyox08ZpqGq5OH59pBk8tnTU76lmLD6eork0fjxmqb7qmI/aW91XXZ4htpuqLBsS13C3VDRljma36oh5xLntTdWPG6OaoPq9LsFCX8XPnRZw7uWeJAGVuqG7OGM1c1ee1AYosfW/UXzeXAyhzTXVLprD5qs9rATR7bMzvOwOUvqq6LVOkharPagK0/mQck3oDlLqiuj1zYKbq05oAbT6rXp41bPCQ4NCpp1WT+wCUuqS6KR10Vn1SA6DdZ9VJmYh2UKKa0heg5EXVkeQ6qz6uDtD+i+oIYuyRoKb0ByhxUX1ZdHCqfm0E0Par6nBi7p6gpvYDqPBcHXhCXQFQ/a3qMIJ5O87asm1B98IBuv5UE1sCjFF3JKhtA3tVhwJkHv3EiK9n5gbo8kM9nw2opp5WrQe0+K2OBCh8yCgvVATolqr2BMr90AuB+sBcdSdAjmNGfasYwGr1j9gOqD0Ci1QfzunZY8ot1b8DTX/pzfQxndeUukClr+qu/AA51qm2AIp802dZY7qgybWBierl3ETMeEBdCxT+ps+zx/S/JtcBdqsDCG2jns8cki1eWS5oQu2wYq/1Zb60ynZFv1QOy/dM3xdMq/Qn1FZh1X/o/VxpxUZ1TdgUdQ9xu6DJtYFOanKnSE2+qiOAQl/jclp/twAyn1N/TiyeKWPBoe/UB/mBcj/1caaYtqvjAVr9VP30/7k3BnsB9FGPE1U9YJD6rBzAgGSjnQiQ/4o6Ayj7PaQJUOSF+n9RgA4Pwl71Aci1T/1RHaiQpOeeqiMARqqeLwqQf9TJ1wmJb89OKgaQa5/qQoCO6pFV6rksAFtVLxQjmL5YlaolMhDMvV/1YPbAHnVh/dfqkkDWf1UvFgtEm/uA6tH8AONVm7BUdTFA1n9UL5aILs8h1SP5AMao7oIC11WXAGT5S/XRgOLZskYu0vWq6uG8AGNUX1YAqj5UXQqQ+U+DH58+ifz+t+qhPABjVT80J1jtgeoygExbArHuywMwTvV9CyJXva+6HCD9mC+xJMzLBjBB9V1zwqvcU11BsMyUE68+R/7y9sy8qgQnqr5tRrSV76muTEcwfY78BSMWyJmBiJNU3zQl+kp3VVenC8RxsurrJsRa8a7qusxxma76ujGxV7ijeqpzqaLFoi5avM0+1VeNiGf52wYT3n+I+v0Pgy8bEt+ylwPxfVKPeBdYnxif1B0lSMNqUw78fznGC4fn1idG"/><span>HoneyHunters</span></a>
            </div>
        </div>
        <div class="mail-logo"></div>
        <div class="comments-form">
            <form id="myForm">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group required">
                            <label for="commentName">Имя</label>
                            <input type="text" class="form-control" name="commentName" id="commentName"
                                   required="required">
                        </div>
                        <div class="form-group required">
                            <label for="exampleInputEmail1">E-Mail</label>
                            <input type="email" class="form-control" name="commentEmail" id="commentEmail"
                                   aria-describedby="emailHelp" required="required">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group required">
                            <label for="exampleInputPassword1">Комментарий</label>
                            <textarea class="form-control" name="commentText" id="commentText"
                                      required="required"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-9 .text-right">
                        <button type="submit" class="custom-button pull-right">Записать</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="comments-list-container">
        <h3>Выводим комментарии</h3>
        <div class="comments-list">

        </div>
    </div>
    <div class="footer">
        <div class="logo">
            <a href="//honey-hunters.ru"><img
                        src="data:image/webp;base64,UklGRioEAABXRUJQVlA4TB4EAAAvMUAMEO/GoG0kR+FP+tqUu38C8z//BJK2eNuvtm0bBl5T6DeAsaAHEkFidERx6BxcL8EMAxOPMAwEi+De40EEX3NbdgzHp01+CgiKtm2btqMR27Zt27Zt27ZZjm1WbNvmYzK+5txz731/ENH/CSDWCv3mLI9+xbyB1UnTapuf/TL2V/81ToMRP4xzyox08ZpqGq5OH59pBk8tnTU76lmLD6eork0fjxmqb7qmI/aW91XXZ4htpuqLBsS13C3VDRljma36oh5xLntTdWPG6OaoPq9LsFCX8XPnRZw7uWeJAGVuqG7OGM1c1ee1AYosfW/UXzeXAyhzTXVLprD5qs9rATR7bMzvOwOUvqq6LVOkharPagK0/mQck3oDlLqiuj1zYKbq05oAbT6rXp41bPCQ4NCpp1WT+wCUuqS6KR10Vn1SA6DdZ9VJmYh2UKKa0heg5EXVkeQ6qz6uDtD+i+oIYuyRoKb0ByhxUX1ZdHCqfm0E0Par6nBi7p6gpvYDqPBcHXhCXQFQ/a3qMIJ5O87asm1B98IBuv5UE1sCjFF3JKhtA3tVhwJkHv3EiK9n5gbo8kM9nw2opp5WrQe0+K2OBCh8yCgvVATolqr2BMr90AuB+sBcdSdAjmNGfasYwGr1j9gOqD0Ci1QfzunZY8ot1b8DTX/pzfQxndeUukClr+qu/AA51qm2AIp802dZY7qgybWBierl3ETMeEBdCxT+ps+zx/S/JtcBdqsDCG2jns8cki1eWS5oQu2wYq/1Zb60ynZFv1QOy/dM3xdMq/Qn1FZh1X/o/VxpxUZ1TdgUdQ9xu6DJtYFOanKnSE2+qiOAQl/jclp/twAyn1N/TiyeKWPBoe/UB/mBcj/1caaYtqvjAVr9VP30/7k3BnsB9FGPE1U9YJD6rBzAgGSjnQiQ/4o6Ayj7PaQJUOSF+n9RgA4Pwl71Aci1T/1RHaiQpOeeqiMARqqeLwqQf9TJ1wmJb89OKgaQa5/qQoCO6pFV6rksAFtVLxQjmL5YlaolMhDMvV/1YPbAHnVh/dfqkkDWf1UvFgtEm/uA6tH8AONVm7BUdTFA1n9UL5aILs8h1SP5AMao7oIC11WXAGT5S/XRgOLZskYu0vWq6uG8AGNUX1YAqj5UXQqQ+U+DH58+ifz+t+qhPABjVT80J1jtgeoygExbArHuywMwTvV9CyJXva+6HCD9mC+xJMzLBjBB9V1zwqvcU11BsMyUE68+R/7y9sy8qgQnqr5tRrSV76muTEcwfY78BSMWyJmBiJNU3zQl+kp3VVenC8RxsurrJsRa8a7qusxxma76ujGxV7ijeqpzqaLFoi5avM0+1VeNiGf52wYT3n+I+v0Pgy8bEt+ylwPxfVKPeBdYnxif1B0lSMNqUw78fznGC4fn1idG"/><span>HoneyHunters</span></a>
        </div>
        <div class="social">
            <a  href="//honey-hunters.ru" class="vk"></a>
            <a  href="//honey-hunters.ru" class="fb"></a>
        </div>
    </div>
</div>
<?php
$this->registerJsFile('js/jquery-3.3.1.min.js');
$this->registerJsFile('js/jquery.validate.min.js');
$this->registerJsFile('js/additional-methods.min.js');
$this->registerJsFile('js/main.js');
?>
