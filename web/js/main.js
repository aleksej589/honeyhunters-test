$(function () {

    /**
     * Add comment
     */
    function submitForm() {
        var name = $('#commentName').val(),
            email = $('#commentEmail').val(),
            text = $('#commentText').val();

        var data = new FormData();
        data.append('name', name);
        data.append('email', email);
        data.append('text', text);
        $.ajax({
            url: '/?r=site/save',
            data: data,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function (data) {
                if (data.success) {
                    clearForm();
                    renderComments(data.comments);
                }
            }

        })
    }

    /**
     * Clear form controls
     */
    function clearForm() {
        $('#commentName').val('');
        $('#commentEmail').val('');
        $('#commentText').val('');
    }

    /**
     * Render comment block
     * @param comments List of comments
     */
    function renderComments(comments) {
        function render(comment) {
            return `<div class="comment"><div class="comment-header">${comment.name}</div><div class="comment-content"><p class="email">${comment.email}</p><p>${comment.text}</p></div></div>`;
        }

        var commentsList = [];
        $.each(comments, function (i, v) {
            commentsList.push($(render(v)));
        });
        $('.comments-list').empty().append(commentsList);
    }

    /**
     * Jquery validation rules
     */
    $('#myForm').validate({
        rules: {
            commentEmail: {
                required: true,
            },
            commentName: {
                required: true,
            },
            commentText: {
                required: true,
            }
        },
        messages: {
            "commentEmail": {
                required: "Обязательное поле",
                email: "Пожалуйста, введите правильный Email адрес"
            },
            "commentName": {
                required: "Обязательное поле"
            },
            "commentText": {
                required: "Обязательное поле"
            }
        },
        submitHandler: function (form) {
            submitForm();
        }
    });

    /**
     * Get comments from server
     */
    (function getComments() {
        $.ajax({
            url: '/?r=site/list',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            type: 'get',
            success: function (data) {
                if (data.success) {
                    renderComments(data.comments);
                }
            }

        })
    }());
})