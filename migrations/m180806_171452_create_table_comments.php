<?php

use yii\db\Migration;

/**
 * Class m180806_171452_create_table_comments
 */
class m180806_171452_create_table_comments extends Migration
{
    public function up()
    {
        $this->createTable('comment', [
            'id' => $this->primaryKey(),
            'email' => $this->string(255),
            'name' => $this->string(255),
            'text' => $this->text()
        ]);
    }

    public function down()
    {
        echo "m180806_171452_create_table_comments cannot be reverted.\n";

        return false;
    }
}
